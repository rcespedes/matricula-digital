import { Directive, HostListener, HostBinding, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[appDnd]'
})
export class DndDirective {
  @HostBinding('class.fileover')
  fileOver: boolean;

  @Output()
  fileDropped = new EventEmitter<any>();;

  constructor() { }


  @HostListener('dragover', ['$event'])
  onDragOver(e) {
    e.preventDefault();
    e.stopPropagation();
    this.fileOver = true;
  }

  @HostListener('dragleave', ['$event'])
  onDragLeave(e) {
    e.preventDefault();
    e.stopPropagation();
    this.fileOver = false;
  }

  @HostListener('drop', ['$event'])
  onDrop(e) {
    e.preventDefault();
    e.stopPropagation();

    this.fileOver = false;
    const files = e.dataTransfer.files;

    if (files.length > 0) {
      this.fileDropped.emit(files);
    }
  }
}
