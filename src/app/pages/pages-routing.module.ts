import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'registrar', loadChildren: () => import('./register/register.module').then(m => m.RegisterModule) },
  { path: 'vacantes', loadChildren: () => import('./vacancies/vacancies.module').then(m => m.VacanciesModule) },
  { path: 'confirmar', loadChildren: () => import('./confirm/confirm.module').then(m => m.ConfirmModule) },
  { path: '', pathMatch: 'full', redirectTo: 'confirmar' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }