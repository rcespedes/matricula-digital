import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-vacancies',
  templateUrl: './vacancies.component.html',
  styleUrls: ['./vacancies.component.scss']
})
export class VacanciesComponent implements OnInit {

  calculateForm: FormGroup
  requestForm: FormGroup

  constructor( private router: Router ) { }

  ngOnInit(): void {
  }

  bindForm(e: FormGroup, opt: number) {
    switch (opt) {
      case 1:
        this.calculateForm = e;
        break;
      case 2:
        this.requestForm = e;
        break;
    }
  }
}
