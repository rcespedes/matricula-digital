import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-calculate',
  templateUrl: './calculate.component.html',
  styleUrls: ['./calculate.component.scss']
})
export class CalculateComponent implements OnInit {

  vacanciesData = [
    {
      title: 'Meta total de atención',
      total: 50,
      collapsed: true,
      detail: [
        {
          title: 'Meta total de atención NNA con NEE',
          quantity: 4,
          editable: false,
          icon: ''
        },
        {
          title: 'Meta total de atención NNA',
          quantity: 46,
          editable: false,
          icon: ''
        }
      ]
    },
    {
      title: 'Estudiantes matriculados en el año/grado anterior',
      total: 48,
      collapsed: true,
      detail: [
        {
          title: 'Total matriculados NNA con NEE',
          quantity: 4,
          editable: false,
          icon: ''
        },
        {
          title: 'Total matriculados NNA',
          quantity: 44,
          editable: false,
          icon: ''
        }
      ]
    },
    {
      title: 'Traslados a otras IIEE del año/grado anterior',
      total: 0,
      collapsed: false,
      detail: [
        {
          title: 'Total traslados NNA con NEE',
          quantity: 0,
          editable: false,
          icon: ''
        },
        {
          title: 'Total traslados NNA',
          quantity: 0,
          editable: false,
          icon: ''
        }
      ]
    },
    {
      title: 'Repitencia del año/grado anterior',
      total: 0,
      collapsed: false,
      detail: [
        {
          title: 'Total repitencias NNA con NEE',
          quantity: 0,
          editable: false,
          icon: ''
        },
        {
          title: 'Total repitencias NNA',
          quantity: 0,
          editable: false,
          icon: ''
        }
      ]
    },
    {
      title: 'Repitencia del año/grado actual',
      total: 0,
      collapsed: false,
      detail: [
        {
          title: 'Total repitencias NNA con NEE',
          quantity: 0,
          editable: true,
          icon: ''
        },
        {
          title: 'Total repitencias NNA',
          quantity: 0,
          editable: true,
          icon: ''
        }
      ]
    }
  ];

  vacanciesTotal = [{
    title: 'Número total de vacantes',
      total: 2,
      collapsed: true,
      detail: [
        {
          title: 'Número total de vacantes NNA con NEE',
          quantity: 0,
          editable: true,
          icon: ''
        },
        {
          title: 'Número total de vacantes NNA',
          quantity: 2,
          editable: true,
          icon: ''
        }
      ]
  }]

  @Output()
  onCalculateFormChange = new EventEmitter<FormGroup>(true)
  calculateForm: FormGroup

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.initForm()
    this.onCalculateFormChange.emit(this.calculateForm)
  }
  initForm() {
    this.calculateForm = this.formBuilder.group({
      ieIntegrada : [{
        value : 'Si',
        disabled : true
      }]
    })
  }

}
