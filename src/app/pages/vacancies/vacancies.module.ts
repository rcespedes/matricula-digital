import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VacanciesRoutingModule } from './vacancies-routing.module';
import { VacanciesComponent } from './vacancies.component';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';
import { CalculateComponent } from './calculate/calculate.component';
import { RequestComponent } from './request/request.component';

// Material imports
import { MatStepperModule } from '@angular/material/stepper';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';


@NgModule({
  declarations: [
    VacanciesComponent,
    CalculateComponent,
    RequestComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FuseSharedModule,

    VacanciesRoutingModule,

    // Material imports
    MatStepperModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule
  ]
})
export class VacanciesModule { }
