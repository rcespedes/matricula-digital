import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfirmRoutingModule } from './confirm-routing.module';
import { FuseSharedModule } from '@fuse/shared.module';

import { ConfirmComponent } from './confirm.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ServicesComponent } from './services/services.component';
import { InfrastructureComponent } from './infrastructure/infrastructure.component';
import { PerformanceComponent } from './performance/performance.component';
import { ApafaComponent } from './apafa/apafa.component';

import { MatCarouselModule } from '@ngmodule/material-carousel';

import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { MatChipsModule } from '@angular/material/chips';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatCheckboxModule } from '@angular/material/checkbox';



@NgModule({
  declarations: [ConfirmComponent, AboutUsComponent, ServicesComponent, InfrastructureComponent, PerformanceComponent, ApafaComponent],
  imports: [
    CommonModule,
    ConfirmRoutingModule,
    FuseSharedModule,

    // Customs
    MatCarouselModule,

    // Material
    MatIconModule,
    MatTabsModule,
    MatChipsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatCheckboxModule
  ]
})
export class ConfirmModule { }
