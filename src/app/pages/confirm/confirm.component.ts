import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {

  school = 'IE 344 Nilda Rojas de Chambilla'
  preview = true

  constructor() { }

  ngOnInit(): void {
  }

}
