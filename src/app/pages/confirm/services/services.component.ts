import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {

  lastYear = new Date().getFullYear() - 1;

  dataSource = {
    morningData: [
      {
        level: 'Inicial',
        age: '03 años',
        students: 50,
        sections: 2,
        average: 25
      },
      {
        level: 'Inicial',
        age: '04 años',
        students: 77,
        sections: 3,
        average: 25
      },
      {
        level: 'Inicial',
        age: '05 años',
        students: 75,
        sections: 4,
        average: 22
      },
      {
        level: 'Primaria',
        age: '06 años',
        students: 68,
        sections: 3,
        average: 22
      }
    ],
    afternoonData: [
      {
        level: 'Inicial',
        age: '03 años',
        students: 50,
        sections: 2,
        average: 25
      },
      {
        level: 'Inicial',
        age: '04 años',
        students: 77,
        sections: 3,
        average: 25
      },
      {
        level: 'Inicial',
        age: '05 años',
        students: 75,
        sections: 4,
        average: 22
      },
      {
        level: 'Primaria',
        age: '06 años',
        students: 68,
        sections: 3,
        average: 22
      }
    ],
  }
  
  servicesData: FormGroup;
  displayedColumns = ['level', 'age', 'students', 'sections', 'average'];

  slides = [
    {
      image: '../../assets/images/demo-content/morain-lake.jpg'
    },
    {
      image: '../../assets/images/demo-content/morain-lake.jpg'
    },
    {
      image: '../../assets/images/demo-content/morain-lake.jpg'
    },
    {
      image: '../../assets/images/demo-content/morain-lake.jpg'
    },
    {
      image: '../../assets/images/demo-content/morain-lake.jpg'
    }
  ]

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.initForm()
  }

  initForm() {
    this.servicesData = this.formBuilder.group({
      deportesDesc : ['', [
        // Validators.required, 
        Validators.maxLength(250)
      ]],
      idiomasDesc : ['', [
        // Validators.required, 
        Validators.maxLength(250)
      ]],
      talleresDesc : ['', [
        // Validators.required, 
        Validators.maxLength(250)
      ]],
      otrosDesc : ['', [
        // Validators.required, 
        Validators.maxLength(250)
      ]],
    })
  }

}
