import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-infrastructure',
  templateUrl: './infrastructure.component.html',
  styleUrls: ['./infrastructure.component.scss']
})
export class InfrastructureComponent implements OnInit {

  slides = [
    {
      image: '../../assets/images/demo-content/morain-lake.jpg'
    },
    {
      image: '../../assets/images/demo-content/morain-lake.jpg'
    },
    {
      image: '../../assets/images/demo-content/morain-lake.jpg'
    },
    {
      image: '../../assets/images/demo-content/morain-lake.jpg'
    },
    {
      image: '../../assets/images/demo-content/morain-lake.jpg'
    }
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
