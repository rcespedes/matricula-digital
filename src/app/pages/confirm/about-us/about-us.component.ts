import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {

  generalDataFields = [
    {
      label: 'Director',
      controlName: 'director'
    },
    {
      label: 'Dirección',
      controlName: 'direccion'
    },
    {
      label: 'Ubicación',
      controlName: 'ubicacion'
    },
    {
      label: 'Teléfono',
      controlName: 'telefono'
    },
    {
      label: 'Correo',
      controlName: 'correo'
    },
    {
      label: 'Página web',
      controlName: 'web'
    },
    {
      label: 'Facebook',
      controlName: 'facebook'
    },
    {
      label: 'YouTube',
      controlName: 'youtube'
    },
    {
      label: 'Modalidad / Nivel',
      controlName: 'modalidad'
    },
    {
      label: 'Gestión - Dependencia',
      controlName: 'dependencia'
    },
    {
      label: 'Área',
      controlName: 'area'
    }
  ]
  identityFields = [
    {
      label: 'Mision',
      controlName: 'mision'
    },
    {
      label: 'Vision',
      controlName: 'vision'
    },
    {
      label: 'Valores',
      controlName: 'valores'
    },
    {
      label: 'Propuesta Pedagógica',
      controlName: 'propuestaPedagogica'
    },
    {
      label: 'Propuesta de Gestón',
      controlName: 'propuestaDeGestion'
    },
  ]

  generalDataForm: FormGroup
  identityForm: FormGroup

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.initForm()
  }

  initForm() {
    this.generalDataForm = this.formBuilder.group({
      director : ['Álvarez Gonzales, Jorge', Validators.required],
      gestion : ['Pública de gestión directa', Validators.required],
      dependencia : ['Pública - Sector Educación', Validators.required],
      area : ['Urbano', Validators.required],
      modalidad : ['Educación Básica Regular', Validators.required],
      ubicacion : ['Tacna, Tacna, Tacna', Validators.required],
      direccion : ['Av. xjasnjdsn.com Nro 345', Validators.required],
      telefono : ['(34) 542145', Validators.required],
      correo : ['some@mail.com', [Validators.required, Validators.email]],
      web : ['www.ie1.pe', [Validators.required]],
      facebook : ['https://es-la.facebook.com/ie1/', Validators.required],
      youtube : ['https://www.youtube.com/watch?v=Hugyg', Validators.required]
    })

    this.identityForm = this.formBuilder.group({
      mision : 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Illum voluptatem non praesentium dolore voluptas, voluptate eum delectus quam excepturi possimus molestiae aut similique, consequatur sed repudiandae eveniet quas earum minima.',
      vision : 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Illum voluptatem non praesentium dolore voluptas, voluptate eum delectus quam excepturi possimus molestiae aut similique, consequatur sed repudiandae eveniet quas earum minima.',
      valores : 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Illum voluptatem non praesentium dolore voluptas, voluptate eum delectus quam excepturi possimus molestiae aut similique, consequatur sed repudiandae eveniet quas earum minima.',
      propuestaPedagogica : 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Illum voluptatem non praesentium dolore voluptas, voluptate eum delectus quam excepturi possimus molestiae aut similique, consequatur sed repudiandae eveniet quas earum minima.',
      propuestaDeGestion : 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Illum voluptatem non praesentium dolore voluptas, voluptate eum delectus quam excepturi possimus molestiae aut similique, consequatur sed repudiandae eveniet quas earum minima.',
    })
  }

}
