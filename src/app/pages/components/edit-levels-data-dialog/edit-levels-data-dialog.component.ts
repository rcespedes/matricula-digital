import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-levels-data-dialog',
  templateUrl: './edit-levels-data-dialog.component.html',
  styles: []
})
export class EditLevelsDataDialogComponent implements OnInit {

  levelData: FormGroup

  constructor(public dialogRef: MatDialogRef<EditLevelsDataDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.initForm()
  }

  initForm() {
    this.levelData = this.formBuilder.group({
      level : [this.data.levelData.level],
      age : [this.data.levelData.age],
      students : [this.data.levelData.students],
      numberOfSections : [this.data.levelData.sections],
      average : [this.data.levelData.average],
    })
  }

}
