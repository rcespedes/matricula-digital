import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { RegisterRoutingModule } from './register-routing.module';

import { RegisterComponent } from './register.component';
import { MatStepperModule } from '@angular/material/stepper';
import { GeneralDataComponent } from './general-data/general-data.component';
import { IdentityComponent } from './identity/identity.component';
import { ServicesComponent } from './services/services.component';
import { InfrastructureComponent } from './infrastructure/infrastructure.component';
import { ApafaComponent } from './apafa/apafa.component';

import { FlexLayoutModule } from '@angular/flex-layout';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { FuseSharedModule } from '@fuse/shared.module';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';



@NgModule({
  declarations: [
    RegisterComponent,
    GeneralDataComponent,
    IdentityComponent,
    ServicesComponent,
    InfrastructureComponent,
    ApafaComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    RegisterRoutingModule,
    FlexLayoutModule,
    FuseSharedModule,

    // Material
    MatStepperModule,
    MatIconModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    MatCheckboxModule,
    MatTableModule,
    MatTabsModule,
    MatTooltipModule
  ]
})
export class RegisterModule { }
