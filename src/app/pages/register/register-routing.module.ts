import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register.component';


const routes: Routes = [
  { 
    path: '', 
    component: RegisterComponent
    // children: [
    //   { path: 'general', loadChildren: () => import('./general-data/general-data.module').then(m => m.GeneralDataModule) },
    //   { path: 'identidad', loadChildren: () => import('./identity/identity.module').then(m => m.IdentityModule) },
    //   { path: 'servicios', loadChildren: () => import('./services/services.module').then(m => m.ServicesModule) },
    //   { path: 'infraestructura', loadChildren: () => import('./infrastructure/infrastructure.module').then(m => m.InfrastructureModule) },
    //   { path: 'apafa', loadChildren: () => import('./apafa/apafa.module').then(m => m.ApafaModule) }
    // ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterRoutingModule { }
