import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-infrastructure',
  templateUrl: './infrastructure.component.html',
  styleUrls: ['./infrastructure.component.scss']
})
export class InfrastructureComponent implements OnInit {

  @Output()
  onInfrastructureDataChange = new EventEmitter<FormGroup>(true)
  infrastructureData: FormGroup;

  files: any[] = [];

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.initForm();
    this.onInfrastructureDataChange.emit(this.infrastructureData);
  }

  initForm() {
    this.infrastructureData = this.formBuilder.group({
      aulasDesc : ['', [
        // Validators.required, 
        Validators.maxLength(250)
      ]],
      laboratoriosDesc : ['', [
        // Validators.required, 
        Validators.maxLength(250)
      ]],
      auditoriosDesc : ['', [
        // Validators.required, 
        Validators.maxLength(250)
      ]],
      bibliotecaDesc : ['', [
        // Validators.required, 
        Validators.maxLength(250)
      ]],
      instalacionesDeportivasDesc : ['', [
        // Validators.required, 
        Validators.maxLength(250)
      ]],
    })
  }

  /**
   * on file drop handler
   */
  onFileDropped($event) {
    
  }

  fileBrowseHandler(files) {
    this.prepareFilesList(files);
  }

  prepareFilesList(files: Array<any>) {
    for (const item of files) {
      this.files.push(item);
    }
  }

}
