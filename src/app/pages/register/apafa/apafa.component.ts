import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-apafa',
  templateUrl: './apafa.component.html',
  styleUrls: ['./apafa.component.scss']
})
export class ApafaComponent implements OnInit {

  @Output()
  onApafaDataChange = new EventEmitter<FormGroup>(true)
  apafaData: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.initForm();
    this.onApafaDataChange.emit(this.apafaData);
  }

  initForm() {
    this.apafaData = this.formBuilder.group({
      queEsApafa : ['', [
        // Validators.required, 
        Validators.maxLength(250)
      ]],
      importancia : ['', [
        // Validators.required, 
        Validators.maxLength(250)
      ]],
      uso : ['', [
        // Validators.required, 
        Validators.maxLength(400)
      ]],
      cuota : [ '',
        Validators.maxLength(6)
      ]
    })
  }

  files: any[] = [];

  onFileDropped($event) {
    this.prepareFilesList($event);
  }

  fileBrowseHandler(files) {
    this.prepareFilesList(files);
  }

  prepareFilesList(files: Array<any>) {
    for (const item of files) {
      this.files.push(item);
    }
  }

}
