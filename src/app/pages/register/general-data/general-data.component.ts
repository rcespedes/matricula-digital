import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-general-data',
  templateUrl: './general-data.component.html',
  styleUrls: ['./general-data.component.scss']
})
export class GeneralDataComponent implements OnInit {

  @Output() 
  onGeneralDataChange = new EventEmitter<FormGroup>(true)
  generalData: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.initForm();
    this.onGeneralDataChange.emit(this.generalData)
  }

  initForm() {
    this.generalData = this.formBuilder.group({
      dre : [{
        value: 'Tacna',
        disabled: true
      }, Validators.required],
      ugel : [{
        value: 'Tacna',
        disabled: true
      }, Validators.required],
      codigoLocal : [{
        value: '32414',
        disabled: true
      }, Validators.required],
      director : [{
        value: 'Álvarez Gonzales, Jorge',
        disabled: true
      }, Validators.required],
      gestion : [{
        value: 'Pública de gestión directa',
        disabled: true
      }, Validators.required],
      dependencia : [{
        value: 'Pública - Sector Educación',
        disabled: true
      }, Validators.required],
      area : [{
        value: 'Urbano',
        disabled: true
      }, Validators.required],
      alumnado : [{
        value: 'Mixto',
        disabled: true
      }, Validators.required],
      modalidad : [{
        value: 'Educación Básica Regular',
        disabled: true
      }, Validators.required],
      ieIntegrada : [{
        value: 'Si',
        disabled: true
      }, Validators.required],
      codigoModular : [{
        value: '8452107 Inicial - Jardín, 8452392 Primaria',
        disabled: true
      }, Validators.required],
      ubicacion : [{
        value: 'Tacna, Tacna, Tacna',
        disabled: true
      }, Validators.required],
      direccion : [{
        value: 'Av. xjasnjdsn.com Nro 345',
        disabled: true
      }, Validators.required],
      telefono : ['(34) 542145', Validators.required],
      correo : ['some@mail.com', [Validators.required, Validators.email]],
      web : ['www.ie1.pe', [Validators.required]],
      facebook : ['https://es-la.facebook.com/ie1/', Validators.required],
      youtube : ['https://www.youtube.com/watch?v=Hugyg', Validators.required]
    });
  }

}
