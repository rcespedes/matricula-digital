import { Component, OnInit, Output, EventEmitter, HostListener, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'app/pages/components/confirm-dialog/confirm-dialog.component';
import { EditLevelsDataDialogComponent } from 'app/pages/components/edit-levels-data-dialog/edit-levels-data-dialog.component';

export interface LevelsAndStudents {
  level: string,
  totalOfStudents: number;
  totalOfSections: number;
  totalAverage: number;
}

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {

  dataSource = {
    morningData: [
      {
        level: 'Inicial',
        age: '03 años',
        students: 50,
        sections: 2,
        average: 25
      },
      {
        level: 'Inicial',
        age: '04 años',
        students: 77,
        sections: 3,
        average: 25
      },
      {
        level: 'Inicial',
        age: '05 años',
        students: 75,
        sections: 4,
        average: 22
      },
      {
        level: 'Primaria',
        age: '06 años',
        students: 68,
        sections: 3,
        average: 22
      }
    ],
    afternoonData: [
      {
        level: 'Inicial',
        age: '03 años',
        students: 50,
        sections: 2,
        average: 25
      },
      {
        level: 'Inicial',
        age: '04 años',
        students: 77,
        sections: 3,
        average: 25
      },
      {
        level: 'Inicial',
        age: '05 años',
        students: 75,
        sections: 4,
        average: 22
      },
      {
        level: 'Primaria',
        age: '06 años',
        students: 68,
        sections: 3,
        average: 22
      }
    ],
  }
  
  displayedColumns = ['level', 'age', 'students', 'sections', 'average'];

  lastYear = new Date().getFullYear() - 1;

  @Output()
  onServicesDataChange = new EventEmitter<FormGroup>(true)
  servicesData: FormGroup;

  constructor(private formBuilder: FormBuilder, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.initForm();
    this.onServicesDataChange.emit(this.servicesData);
  }

  initForm() {
    this.servicesData = this.formBuilder.group({
      deportesDesc : ['', [
        // Validators.required, 
        Validators.maxLength(250)
      ]],
      idiomasDesc : ['', [
        // Validators.required, 
        Validators.maxLength(250)
      ]],
      talleresDesc : ['', [
        // Validators.required, 
        Validators.maxLength(250)
      ]],
      otrosDesc : ['', [
        // Validators.required, 
        Validators.maxLength(250)
      ]],
    })
  }

  files: any[] = [];

  onFileDropped($event) {
    this.prepareFilesList($event);
  }

  fileBrowseHandler(files) {
    this.prepareFilesList(files);
  }

  prepareFilesList(files: Array<any>) {
    for (const item of files) {
      this.files.push(item);
    }
  }

  confirmarDatos(e) {
    const confirmDialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '300px',
      data: {level: e.age}
    });

    confirmDialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        alert('Confirmado')
      }
    });
  }

  editarDatos(e) {
    const confirmDialogRef = this.dialog.open(EditLevelsDataDialogComponent, {
      width: '450px',
      data: {levelData: e}
    });

    confirmDialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        alert('Confirmado')
      }
    });
  }

}

