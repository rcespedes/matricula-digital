import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {showError: true}
  }]
})
export class RegisterComponent implements OnInit {

  generalDataForm: FormGroup
  identityDataForm: FormGroup
  servicesDataForm: FormGroup
  infrastructureDataForm: FormGroup
  apafaDataForm: FormGroup

  constructor() { }

  ngOnInit(): void {
  }

  bindForm(e: FormGroup, opt: number) {
    switch (opt) {
      case 1:
        this.generalDataForm = e;
        break;
      case 2:
        this.identityDataForm = e;
        break;
      case 3:
        this.servicesDataForm = e;
        break;
      case 4:
        this.infrastructureDataForm = e;
        break;
      case 5:
        this.apafaDataForm = e;
        break;
    }
  }

  saveForm(f: FormGroup) {
    console.log(f.getRawValue());
  }

}
