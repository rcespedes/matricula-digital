import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-identity',
  templateUrl: './identity.component.html',
  styleUrls: ['./identity.component.scss']
})
export class IdentityComponent implements OnInit {

  @Output()
  onIdentityDataChange = new EventEmitter<FormGroup>(true)
  identityData: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.initForm();
    this.onIdentityDataChange.emit(this.identityData);
  }

  initForm() {
    this.identityData = this.formBuilder.group({
      mision : ['', [
        Validators.required, 
        Validators.maxLength(250)
      ]],
      vision : ['', [
        Validators.required, 
        Validators.maxLength(250)
      ]],
      valores : ['', [
        Validators.required, 
        Validators.maxLength(250)
      ]],
      propuestaPedagogica : ['', [
        Validators.required, 
        Validators.maxLength(400)
      ]],
      propuestaDeGestion : ['', [
        Validators.required, 
        Validators.maxLength(400)
      ]],
    })
  }

  files: any[] = [];

  onFileDropped($event) {
    this.prepareFilesList($event);
  }

  fileBrowseHandler(files) {
    this.prepareFilesList(files);
  }

  prepareFilesList(files: Array<any>) {
    for (const item of files) {
      this.files.push(item);
    }
  }

}
