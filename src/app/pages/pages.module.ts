import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Custom Modules
import { PagesRoutingModule } from './pages-routing.module';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { EditLevelsDataDialogComponent } from './components/edit-levels-data-dialog/edit-levels-data-dialog.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FuseSharedModule } from '@fuse/shared.module';



@NgModule({
  declarations: [ConfirmDialogComponent, EditLevelsDataDialogComponent],
  imports: [
    CommonModule,
    PagesRoutingModule,
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    FuseSharedModule
  ]
})
export class PagesModule { }
