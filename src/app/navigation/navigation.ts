import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id       : 'menu',
        title    : 'Menu',
        type     : 'group',
        children : [
            {
                'id'       : 'registrar-informacion',
                'title'    : 'Registro de Información',
                'type'     : 'item',
                'icon'     : 'assignment',
                'url'      : '/matricula/registrar'
            },
            {
                'id'       : 'calculo-vacantes',
                'title'    : 'Cálculo de Vacantes',
                'type'     : 'item',
                'icon'     : 'person_add',
                'url'      : '/matricula/vacantes'
            },
            {
                'id'       : 'confirma-informacion',
                'title'    : 'Confirma tu Información',
                'type'     : 'item',
                'icon'     : 'assignment_turned_in',
                'url'      : '/matricula/confirmar'
            },
            {
                'id'       : 'visualizar-resultados',
                'title'    : 'Resultados',
                'type'     : 'item',
                'icon'     : 'visibility',
                'url'      : '/matricula/resultados'
            }
        ]
    }
];
